package com.example.rotfl.rejestrv11;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import org.apache.http.Header;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by rotfl on 29.10.2015.
 */
public class RejestrActivity extends Activity{

    // url to get all products list
      AsyncHttpClient client;
    String idSamochodu;
    String idUzytkownika;
    static String url_all_products;
    TextView n ;
    Button b;
    int  mHour;
    int mMinute;
    int mYear;
    int mMonth;
    int mDay;
    String time;
    String date;
    String datetime;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rezerwacjepart2_layout);
    //   n = (TextView)findViewById(R.id.nazwaSamochodu);

//pobieranie danych z MainActivity
        Intent i = getIntent();
        Bundle przekazDane = i.getExtras();
        idSamochodu = przekazDane.getString("ids");
        idUzytkownika = przekazDane.getString("idUzytkownika1");
//

        ConfigClass cc = new ConfigClass();
        url_all_products = cc.getIpAdress()+"/WidokRezerwacje.php?idsamochodu="+idSamochodu;

        b = (Button) findViewById(R.id.button2);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(idUzytkownika!=null) {
                    Intent i = new Intent(getApplicationContext(), RezerwujSActivity.class);
                    i.putExtra("ids", idSamochodu);
                    i.putExtra("idUzytkownika1", idUzytkownika);//wysyła idUzytkownika z RezerwuSActivity
                    finish();
                    startActivity(i);
                }else{
                    b.setText("Musisz być zalogowany!");
                }
            }
        });



       // final ListView listView = (ListView) findViewById(android.R.id.list);
        final ListView lv  = (ListView) findViewById(R.id.list);
        AsyncHttpClient client = new AsyncHttpClient();
       //  client.post(RejestrActivity.this, url_all_products, new AsyncHttpResponseHandler(){


//wczytanie danych z tabeli rezerwacje---------------------------------------------------------------
        RequestHandle requestHandle = client.get(url_all_products, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                InputStream inputStream = new ByteArrayInputStream(responseBody);
                ArrayList<Rezerwacje> rezerwacjes = new ArrayList();
                try {
                    inputStream.read();
                    inputStream.close();

                    String text = new String(responseBody);

                    Gson gson = new Gson();
                    JsonParser jsonParser = new JsonParser();
                    JsonArray jsonArray = jsonParser.parse(text).getAsJsonArray();

                    for (JsonElement jsonElement : jsonArray) {
                        Rezerwacje rezerwacje = gson.fromJson(jsonElement, Rezerwacje.class);
                        rezerwacjes.add(rezerwacje);

                    }

                    RezerwacjeAdapter rezerwacjeAdapter = new RezerwacjeAdapter(getBaseContext(), rezerwacjes);
                    lv.setAdapter(rezerwacjeAdapter);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }

        });
//-------------------------------------------------------------------------------------------------------
    }


//metoda umozliwia odczytywanie daty i godziny z DatePicterDialog i TimePickerDialog
    public void showDateTime() {
        //wyswietlenie i pobranie roku-miesiaca-dnia

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        date = year + "-" + dayOfMonth + "-" + (monthOfYear + 1);

                    }
                }, mYear, mMonth, mDay);
        dpd.show();
        //wyswietlenie i pobranie godziny i minuty

        TimePickerDialog tpd = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        time = hourOfDay + ":"+minute;

                    }
                }, mHour, mMinute, false);
        tpd.show();
        datetime = date + " " + time;

    }
//koniec metody showDateTime


    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

