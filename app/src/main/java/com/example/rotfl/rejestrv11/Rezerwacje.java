package com.example.rotfl.rejestrv11;

/**
 * Created by rotfl on 29.10.2015.
 */
public class Rezerwacje {

    /**
     * imie : Pawe?
     * nazwisko : Kowalski
     * Nazwa : Seat Leon
     * Nr_Rejestracyjny : GND 6B475
     * Id_Samochodu : 4
     * Id_Uzytkownika : 2
     * Data_Rezerwacji_Od : 2015-11-11 10:15:00
     * Data_Rezerwacji_Do : 2015-11-17 10:54:23
     */

    private String imie;
    private String nazwisko;
    private String Nazwa;
    private String Nr_Rejestracyjny;
    private String Id_Samochodu;
    private String Id_Uzytkownika;
    private String Data_Rezerwacji_Od;
    private String Data_Rezerwacji_Do;

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setNazwa(String Nazwa) {
        this.Nazwa = Nazwa;
    }

    public void setNr_Rejestracyjny(String Nr_Rejestracyjny) {
        this.Nr_Rejestracyjny = Nr_Rejestracyjny;
    }

    public void setId_Samochodu(String Id_Samochodu) {
        this.Id_Samochodu = Id_Samochodu;
    }

    public void setId_Uzytkownika(String Id_Uzytkownika) {
        this.Id_Uzytkownika = Id_Uzytkownika;
    }

    public void setData_Rezerwacji_Od(String Data_Rezerwacji_Od) {
        this.Data_Rezerwacji_Od = Data_Rezerwacji_Od;
    }

    public void setData_Rezerwacji_Do(String Data_Rezerwacji_Do) {
        this.Data_Rezerwacji_Do = Data_Rezerwacji_Do;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public String getNazwa() {
        return Nazwa;
    }

    public String getNr_Rejestracyjny() {
        return Nr_Rejestracyjny;
    }

    public String getId_Samochodu() {
        return Id_Samochodu;
    }

    public String getId_Uzytkownika() {
        return Id_Uzytkownika;
    }

    public String getData_Rezerwacji_Od() {
        return Data_Rezerwacji_Od;
    }

    public String getData_Rezerwacji_Do() {
        return Data_Rezerwacji_Do;
    }
}
