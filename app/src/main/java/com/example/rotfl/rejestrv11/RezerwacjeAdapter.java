package com.example.rotfl.rejestrv11;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by rotfl on 29.10.2015.
 */
public class RezerwacjeAdapter extends BaseAdapter {

    Context context;
    ArrayList<Rezerwacje> listaRezerwacji;

    public RezerwacjeAdapter(Context context, ArrayList<Rezerwacje> listaRezerwacji) {
        this.context = context;
        this.listaRezerwacji = listaRezerwacji;
    }
    public RezerwacjeAdapter() {

    }

    public int getCount() {
        return listaRezerwacji.size();
    }

    public Object getItem(int position) {
        return listaRezerwacji.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }


//Layout methods



    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);

        //Przypisanie wartosci do View

        View rowView = inflater.inflate(R.layout.rezerwacje_layout, parent, false);
        Rezerwacje rezerwacje = (Rezerwacje)getItem(position);
//        ImageView imageView = (ImageView) rowView.findViewById(R.id.imageImageView);
//        Picasso.with(context).load(samochody.getUrl()).into(imageView);
        TextView nazwaS = (TextView)rowView.findViewById(R.id.nazwaSamochodu);
        TextView imie = (TextView)rowView.findViewById(R.id.imie);
        TextView data_od = (TextView) rowView.findViewById(R.id.data_od);


        nazwaS.setText(rezerwacje.getNazwa() + " " + rezerwacje.getNr_Rejestracyjny());
        imie.setText(rezerwacje.getImie() + " " + rezerwacje.getNazwisko());
        data_od.setText(rezerwacje.getData_Rezerwacji_Od()+" - " +rezerwacje.getData_Rezerwacji_Do());


        return rowView;
    }

}
