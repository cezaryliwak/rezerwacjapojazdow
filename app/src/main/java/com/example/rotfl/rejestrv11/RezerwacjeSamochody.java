package com.example.rotfl.rejestrv11;

/**
 * Created by rotfl on 03.11.2015.
 */
public class RezerwacjeSamochody {


    /**
     * Id : 60
     * Id_Uzytkownika : 1
     * Id_Samochodu : 1
     * Data_Rezerwacji_Od : 2015-11-19 09:53:50
     * Data_Rezerwacji_Do : 2015-11-19 09:55:50
     * Nazwa : Scoda Octavia
     * Nr_Rejestracyjny : NE 12345
     * imie : Cezary
     * nazwisko : Liwak
     */

    private String Id;
    private String Id_Uzytkownika;
    private String Id_Samochodu;
    private String Data_Rezerwacji_Od;
    private String Data_Rezerwacji_Do;
    private String Nazwa;
    private String Nr_Rejestracyjny;
    private String imie;
    private String nazwisko;

    public void setId(String Id) {
        this.Id = Id;
    }

    public void setId_Uzytkownika(String Id_Uzytkownika) {
        this.Id_Uzytkownika = Id_Uzytkownika;
    }

    public void setId_Samochodu(String Id_Samochodu) {
        this.Id_Samochodu = Id_Samochodu;
    }

    public void setData_Rezerwacji_Od(String Data_Rezerwacji_Od) {
        this.Data_Rezerwacji_Od = Data_Rezerwacji_Od;
    }

    public void setData_Rezerwacji_Do(String Data_Rezerwacji_Do) {
        this.Data_Rezerwacji_Do = Data_Rezerwacji_Do;
    }

    public void setNazwa(String Nazwa) {
        this.Nazwa = Nazwa;
    }

    public void setNr_Rejestracyjny(String Nr_Rejestracyjny) {
        this.Nr_Rejestracyjny = Nr_Rejestracyjny;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getId() {
        return Id;
    }

    public String getId_Uzytkownika() {
        return Id_Uzytkownika;
    }

    public String getId_Samochodu() {
        return Id_Samochodu;
    }

    public String getData_Rezerwacji_Od() {
        return Data_Rezerwacji_Od;
    }

    public String getData_Rezerwacji_Do() {
        return Data_Rezerwacji_Do;
    }

    public String getNazwa() {
        return Nazwa;
    }

    public String getNr_Rejestracyjny() {
        return Nr_Rejestracyjny;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }
}
