package com.example.rotfl.rejestrv11;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by rotfl on 04.11.2015.
 */
public class RezerwacjeSamochodyAdapter {

    Context context;
    ArrayList<RezerwacjeSamochody> listaRezerwacjiSamochodow;


    public RezerwacjeSamochodyAdapter(Context context, ArrayList<RezerwacjeSamochody> listaRezerwacjiSamochodow) {
        this.context = context;
        this.listaRezerwacjiSamochodow = listaRezerwacjiSamochodow;
    }
    public int getCount() {
        return listaRezerwacjiSamochodow.size();
    }

    public Object getItem(int position) {
        return listaRezerwacjiSamochodow.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }


}
