package com.example.rotfl.rejestrv11;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import org.apache.http.Header;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;


/**
 * Created by rotfl on 30.10.2015.
 */
public class RezerwujSActivity extends  Activity{

    AsyncHttpClient client;
    TextView getId;
    TextView uwagi;
    String id_uzytkownika;
    Button b;
    Button rezButton;
    Button oddButton;
    Button naGodzineButton;
    String idSamochodu;
    String idUzytkownika;
    String url;
    int  mHourZajecia;
    int  mHourOddania;
    int mMinute;
    int mYear = 2016;
    int mMonth;
    int mDay;
    String timeZ;
    String dateZ;
    String timeO;
    String dateO;
    String datetimeZajecia;
    String datetimeOddania;
    private Toast toast;
    ArrayList<WolneSamochody> wolneSamochodies;
    WolneSamochodyAdapter adapter ;
    String a  ;
    String a1 ;
    String c;
    String c1;
    String c2;
    int c3;
    MainActivity main = new MainActivity();
    ConfigClass cc = new ConfigClass();
    private static String url_wszystkei_folne ;

        public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
            setContentView(R.layout.rsamochod_layout);
            url_wszystkei_folne = cc.getIpAdress()+"/WyswietlZajete.php";
            b = (Button) findViewById(R.id.button3);
            rezButton = (Button) findViewById(R.id.rezButton);
            oddButton = (Button) findViewById(R.id.oddButton);
            naGodzineButton = (Button) findViewById(R.id.naGodzineButton);
            Intent i = getIntent();
            Bundle przekazDane = i.getExtras();
            idSamochodu = przekazDane.getString("ids");
            idUzytkownika = przekazDane.getString("idUzytkownika1");
            uwagi = (TextView) findViewById(R.id.uwagi);


            client = new AsyncHttpClient();



            AsyncHttpClient client2 = new AsyncHttpClient();

            client2.get(this, url_wszystkei_folne, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    InputStream inputStream = new ByteArrayInputStream(responseBody);
                    wolneSamochodies = new ArrayList();
                    try {
                        System.out.println("jestem w try");
                        inputStream.read();
                        inputStream.close();

                        String text = new String(responseBody);

                        Gson gson = new Gson();
                        com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
                        JsonArray jsonArray = jsonParser.parse(text).getAsJsonArray();

                        for (JsonElement jsonElement : jsonArray) {
                            WolneSamochody wSamochody = gson.fromJson(jsonElement, WolneSamochody.class);
                            wolneSamochodies.add(wSamochody);
                        }
                        for(WolneSamochody ws : wolneSamochodies){
                            if(ws.getId_Samochodu().equals(idSamochodu)){
                                System.out.println("jestem w if");
                                naGodzineButton.setVisibility(View.INVISIBLE);
                            }
//**************************************************************************************************
//********************Wykonanie Inserta z godzinnym czasem rezerwacji*******************************
                            else{
                                naGodzineButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        System.out.println("przycisk dziala");
                                        url =cc.getIpAdress()+"/sprawdzDostepnosc.php?datarezerwacjiod="+a+"&godzinarezerwacjiod="+a1+"&datarezerwacjido="+c + "&godzinarezerwacjido="+c1+"&idsamochodu="+idSamochodu+ "   czy zajety"  + main.czyZajety;
                                        System.out.println(" "+ url+ " ");
                                            final RequestHandle requestHandle = client.get(url, new AsyncHttpResponseHandler() {
                                                @Override
                                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                                    InputStream inputStream = new ByteArrayInputStream(responseBody);
                                                    try {
                                                        inputStream.read();
                                                        inputStream.close();

                                                        String text = new String(responseBody);

                                                        wolneSamochodies = new ArrayList<>();

                                                        Gson gson = new Gson();
                                                        com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
                                                        JsonArray jsonArray = jsonParser.parse(text).getAsJsonArray();

                                                        for (JsonElement jsonElement : jsonArray) {
                                                            WolneSamochody wolneSamochody = gson.fromJson(jsonElement, WolneSamochody.class);
                                                            wolneSamochodies.add(wolneSamochody);
                                                        }
                                                        adapter = new WolneSamochodyAdapter(getApplicationContext(), wolneSamochodies);
//Ten if sprawdza czy samochod, który chcemy zarezerwować na godzinę nie jes aktualnie uzywany, lub czy nie będzie używany przez kogos innego w ciągu tej godziny

                                                        if(wolneSamochodies.size()==0){

//Pobranie aktualnej daty i czasu oraz ustawienie godziny o 1 wiekszą od obecnej  oraz uruchomienie inserta zapisującego dane***********************************

                                                            SimpleDateFormat dataOd = new SimpleDateFormat("yyyy-MM-dd");
                                                            dataOd.setTimeZone(TimeZone.getTimeZone("Europe/Warsaw"));

                                                            SimpleDateFormat godzinaOd = new SimpleDateFormat("kk:mm:ss");
                                                            godzinaOd.setTimeZone(TimeZone.getTimeZone("Europe/Warsaw"));

                                                            SimpleDateFormat dataDo = new SimpleDateFormat("yyyy-MM-dd");
                                                            dataOd.setTimeZone(TimeZone.getTimeZone("Europe/Warsaw"));

                                                            SimpleDateFormat godzinaDo = new SimpleDateFormat("kk");
                                                            godzinaOd.setTimeZone(TimeZone.getTimeZone("Europe/Warsaw"));

                                                            SimpleDateFormat minutaDo = new SimpleDateFormat("mm:ss");
                                                            godzinaOd.setTimeZone(TimeZone.getTimeZone("Europe/Warsaw"));
                                                            //--------------data i godzina od--------------------
                                                            a  = dataOd.format(new Date());
                                                            a1  = godzinaOd.format(new Date());
                                                            //--------------------------------------------------
                                                            //--------------data, godzina i minuty do------------
                                                            c = dataDo.format(new Date());
                                                            c1 = godzinaDo.format(new Date());  c3 = Integer.parseInt(c1.toString())+1;
                                                            c2 = minutaDo.format(new Date());

                                                            c1  = String.valueOf(c3);   //-- obecna godzina + 1h
                                                            c1 = c1+":"+c2;
                                                            //---------------------------------------------------
                                                            naGodzineButton.setText("Zapisywanie . . .");
                                                            insertInto(idSamochodu, idUzytkownika, a, a1, c, c1);
                                                        }else{
                                                            uwagi.setTextColor(Color.RED);

                                                            uwagi.setText("Ktoś inny będzie używał samochodu w ciągu najbliższej godziny");
                                                        }
                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                                @Override
                                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                                }

                                            });
                                        }
                                    });
                                }
                            }
//**********************************************************************************************************************************************************
//Jesli nei ma zajętych samochodów wtedy poniższy if pozwala na to aby przycisk zadziałał prawidłowo - bez tego niżej przycisk nie wykonywał zadnej operacji
//**********************************************************************************************************************************************************
                        if(wolneSamochodies.size()==0){
                            System.out.println("jestem w elese");
                            naGodzineButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    System.out.println("przycisk dziala");
                                    url =cc.getIpAdress()+"/sprawdzDostepnosc.php?datarezerwacjiod="+a+"&godzinarezerwacjiod="+a1+"&datarezerwacjido="+c + "&godzinarezerwacjido="+c1+"&idsamochodu="+idSamochodu+ "   czy zajety"  + main.czyZajety;
                                    System.out.println(" "+ url+ " ");
                                    final RequestHandle requestHandle = client.get(url, new AsyncHttpResponseHandler() {
                                        @Override
                                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                            InputStream inputStream = new ByteArrayInputStream(responseBody);
                                            try {
                                                inputStream.read();
                                                inputStream.close();

                                                String text = new String(responseBody);

                                                wolneSamochodies = new ArrayList<>();

                                                Gson gson = new Gson();
                                                com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
                                                JsonArray jsonArray = jsonParser.parse(text).getAsJsonArray();

                                                for (JsonElement jsonElement : jsonArray) {
                                                    WolneSamochody wolneSamochody = gson.fromJson(jsonElement, WolneSamochody.class);
                                                    wolneSamochodies.add(wolneSamochody);
                                                }
                                                adapter = new WolneSamochodyAdapter(getApplicationContext(), wolneSamochodies);
//Ten if sprawdza czy samochod, który chcemy zarezerwować na godzinę nie jes aktualnie uzywany, lub czy nie będzie używany przez kogos innego w ciągu tej godziny

                                                if(wolneSamochodies.size()==0){

//Pobranie aktualnej daty i czasu oraz ustawienie godziny o 1 wiekszą od obecnej  oraz uruchomienie inserta zapisującego dane***********************************

                                                    SimpleDateFormat dataOd = new SimpleDateFormat("yyyy-MM-dd");
                                                    dataOd.setTimeZone(TimeZone.getTimeZone("Europe/Warsaw"));

                                                    SimpleDateFormat godzinaOd = new SimpleDateFormat("kk:mm:ss");
                                                    godzinaOd.setTimeZone(TimeZone.getTimeZone("Europe/Warsaw"));

                                                    SimpleDateFormat dataDo = new SimpleDateFormat("yyyy-MM-dd");
                                                    dataOd.setTimeZone(TimeZone.getTimeZone("Europe/Warsaw"));

                                                    SimpleDateFormat godzinaDo = new SimpleDateFormat("kk");
                                                    godzinaOd.setTimeZone(TimeZone.getTimeZone("Europe/Warsaw"));

                                                    SimpleDateFormat minutaDo = new SimpleDateFormat("mm:ss");
                                                    godzinaOd.setTimeZone(TimeZone.getTimeZone("Europe/Warsaw"));
                                                    //--------------data i godzina od--------------------
                                                    a  = dataOd.format(new Date());
                                                    a1  = godzinaOd.format(new Date());
                                                    //--------------------------------------------------
                                                    //--------------data, godzina i minuty do------------
                                                    c = dataDo.format(new Date());
                                                    c1 = godzinaDo.format(new Date());  c3 = Integer.parseInt(c1.toString())+1;
                                                    c2 = minutaDo.format(new Date());

                                                    c1  = String.valueOf(c3);   //-- obecna godzina + 1h
                                                    c1 = c1+":"+c2;
                                                    //---------------------------------------------------
                                                    naGodzineButton.setText("Zapisywanie . . .");
                                                    insertInto(idSamochodu, idUzytkownika, a, a1, c, c1);
                                                }else{
                                                    uwagi.setTextColor(Color.RED);

                                                    uwagi.setText("Ktoś inny będzie używał samochodu w ciągu najbliższej godziny");
                                                }
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                        }

                                    });
                                }
                            });
                        }
//**********************************************************************************************************************************************************
//**********************************************************************************************************************************************************
                        } catch (IOException e) {
                            e.printStackTrace();
                    }
                }
                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                }
            });


//**************************************************************************************************
//****************************Przycisk rezerwujący samochód na wybraną datę*************************
//**************************************************************************************************
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {//re
                        url =cc.getIpAdress()+"/RezerwacjaSamochodu.php?idsamochodu="+idSamochodu+"&iduzytkownika="+idUzytkownika+"&datarezerwacjiod="+dateZ+"&godzinarezerwacjiod="+timeZ+"&datarezerwacjido="+dateO+"&godzinarezerwacjido="+timeO;
                        if(dateO !=null && dateZ!=null && timeO!=null && timeZ!=null) {
                            final RequestHandle requestHandle = client.get(url, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    InputStream inputStream = new ByteArrayInputStream(responseBody);
                                    try {

                                        inputStream.read();
                                        inputStream.close();
                                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                        finish();
                                        startActivity(i);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                }
                            });
                        }else{
                            uwagi.setText("Musisz wybrać datę i godzinę zajęcia/oddania samochodu!");
                        }
                        datetimeZajecia = dateZ + " " + timeZ;
                        datetimeOddania = dateO + " " + timeO;
                    }
                });

//***************************************************************************************************
//*****************Przyciski do pobierania daty i czasu wybrane przez użytkownika********************
//*******rez- data i czas rezerwacji samochody**********odd - data i czas oddania samochodu**********
            rezButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showDateTimeZajecia();
                }
            });
            oddButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDateTimeOddania();
                }
            });
        }


    //***************************************************************************************************
    //*************Funkcja zapisująca rezerwację samochodu na jedną ogdzinę******************************

    public void insertInto(String idSamochodu,String idUzytkownika,String a,String a1,String c,String c1){
        url =cc.getIpAdress()+"/RezerwacjaSamochodu.php?idsamochodu="+idSamochodu+"&iduzytkownika="+idUzytkownika+"&datarezerwacjiod="+a+"&godzinarezerwacjiod="+a1+"&datarezerwacjido="+c+"&godzinarezerwacjido="+c1;
        final RequestHandle requestHandle = client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                InputStream inputStream = new ByteArrayInputStream(responseBody);
                try {
                    inputStream.read();
                    inputStream.close();
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    finish();
                    startActivity(i);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });
    }




    //metoda umozliwia odczytywanie daty i godziny z DatePicterDialog i TimePickerDialog
    public void showDateTimeZajecia() {
        //wyswietlenie i pobranie roku-miesiaca-dnia

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        dateZ = year+ "-" + (monthOfYear + 1) + "-" + dayOfMonth ;
                    }
                }, mYear, mMonth, mDay);

        //wyswietlenie i pobranie godziny i minuty

        TimePickerDialog tpd = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        timeZ = hourOfDay + ":"+minute;
                    }
                }, mHourZajecia, mMinute, false);
        dpd.show();
        tpd.show();

    }
    //koniec metody showDateTime
    public void showDateTimeOddania() {
        //wyswietlenie i pobranie roku-miesiaca-dnia

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        dateO = year+ "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                    }
                }, mYear, mMonth, mDay);

        //wyswietlenie i pobranie godziny i minuty

        TimePickerDialog tpd = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        timeO = hourOfDay + ":"+minute;
                    }
                }, mHourZajecia, mMinute, false);
        dpd.show();
        tpd.show();


    }





    public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }
    }



