package com.example.rotfl.rejestrv11;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.apache.http.Header;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by rotfl on 03.11.2015.
 */
public class RssPullService extends Service implements Serializable {
    @Nullable
    private Toast toast;
    private Timer timer;
    private TimerTask timerTask;



    RezerwacjeSamochodyAdapter adapter1;
    ArrayList<RezerwacjeSamochody> rezerwaciesSamochodu;


    Date dataZakonczenia;
    Date dataObecna;
    int powiadomienie;
    String idUzytkownika;
    private int licznik = 0;
    String dataOddania;
    String idSamochodu;
    String imie;
    String nazwisko;
    String nRejestracyjny;

    DateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm");
    AsyncHttpClient client = new AsyncHttpClient();
    String url_all_products1 = "http://192.168.35.68/web_service/Rezerwacje.php";

    loginActivity la = new loginActivity();




    //--------------------------Początek MyTimeTask------------------------------------------------------------------------
    public class MyTimerTask extends TimerTask {
        public MyTimerTask(){

        }
        public void run() {
//tu tworzymy zadanie do cyklicznego wykonania
            try {
                    wczytaj();
                    //showToast("your service is started");
                    licznik++;
                   // createNotification("testowe powiadomienie");
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
//    --------------------koniec MyTimeTask---------------------------------------------------------------------------------------
    private void showToast(String text) {
        toast.setText(text);
        toast.show();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void writeToLogs(String message) {
        Log.d("HelloServices", message);
    }

    public void onCreate() {
        super.onCreate();
        writeToLogs("Called onCreate() method");
        timer = new Timer();
        toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        writeToLogs("Called onStartCommand() method");

        //inicjalizujemy odpowiednie obiekty (odbywa się to raz – tylko w momencie tworzenia Usługi).
        clearTimerSchedule();   //--->anulujemy istiejace zadanie i resetujemy Timer
        initTask();             //---> tworzymy nowy obiekt naszego zadania cyklicznego
//        Looper.prepare();
        timer.scheduleAtFixedRate(timerTask, 5 * 1000, 5 * 1000);  //wstawiamy nasze zadanie do timera

        return super.onStartCommand(intent, flags, startId);
    }

    private void clearTimerSchedule() {
        if (timerTask != null) {
            timerTask.cancel();
            timer.purge();
        }
    }

    private void initTask() {
        timerTask = new MyTimerTask();
    }

    public void onDestroy() {
        writeToLogs("Called onDestroy method");
        clearTimerSchedule();
        showToast("Wylogowano");

        super.onDestroy();
    }



    public void wczytaj() throws ParseException {

    //pobranie id uzytkownika z loginActivity-------------------------------------------------------------
        String idUzytkownikaZalogowanego;
        idUzytkownikaZalogowanego= la.idUzytkownikaZalogowanego;

    //----------------------------------------------------------------------------------------------------
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Warsaw"));
        dataObecna = df.parse((simpleDateFormat.format(new Date())).toString());
        if (licznik == 0) {

            dataZakonczenia = df.parse(simpleDateFormat.format(new Date()).toString());
        }

//--warunki decydujące w którym momencie wysyłane będzie powidomienie o kończącym się czasie rezerwacji samochodu
        if(idUzytkownika!=null) {
            if (licznik != 0 && powiadomienie == 0) {
                if (dataObecna.equals(dataZakonczenia)) {
                    powiadomienie++;
                    if (powiadomienie == 1) {
                        if (idUzytkownika.equals(idUzytkownikaZalogowanego)) {
                            createNotification("Samochod musi być oddany za 10 minut! ");//dla uzytkownika zalogowanego przyjdzie powiadomieie o
                            //tym ze ma zwolnić samochód w ciągu 10 minut
                        }
                        if (!idUzytkownika.equals(idUzytkownikaZalogowanego)) {
                            createNotification(imie + " "+nazwisko+" odda samochod " + nRejestracyjny +" zostanie oddany");//dla uzytkownika zalogowanego przyjdzie powiadomieie o
                            //tym ze samochód się zwolni w ciągu 10 min
                        }
                    }
                }
            } else {
                powiadomienie = 0;
            }
        }


//-----------------------------------------------------------------------------------------------------------------
//------------------------Zapisanie do listy pierwszego elementu z listy zarezerwowanych samochodów---------------
            client = new SyncHttpClient();
            client.get(RssPullService.this, url_all_products1, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    InputStream inputStream = new ByteArrayInputStream(responseBody);
                    rezerwaciesSamochodu = new ArrayList();
                    try {
                        inputStream.read();
                        inputStream.close();

                        String text = new String(responseBody);

                        Gson gson = new Gson();
                        com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
                        JsonArray jsonArray = jsonParser.parse(text).getAsJsonArray();

                        for (JsonElement jsonElement : jsonArray) {
                            RezerwacjeSamochody samochodyRezerwacje = gson.fromJson(jsonElement, RezerwacjeSamochody.class);

                            rezerwaciesSamochodu.add(samochodyRezerwacje);
                        }
                        adapter1 = new RezerwacjeSamochodyAdapter(getApplicationContext(), rezerwaciesSamochodu);
                        try {
                            if(rezerwaciesSamochodu.size()>0) {
                                dataZakonczenia = df.parse(rezerwaciesSamochodu.get(0).getData_Rezerwacji_Do().toString());
                                idUzytkownika = rezerwaciesSamochodu.get(0).getId_Uzytkownika();
                                idSamochodu = rezerwaciesSamochodu.get(0).getId_Samochodu();
                                dataOddania = rezerwaciesSamochodu.get(0).getData_Rezerwacji_Do();
                                imie = rezerwaciesSamochodu.get(0).getImie();
                                nazwisko = rezerwaciesSamochodu.get(0).getNazwisko();
                                nRejestracyjny = rezerwaciesSamochodu.get(0).getNr_Rejestracyjny();

                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                }
            });
        }



    //Metoda uruchamiająca powiadomienie-----------------------------------------------
    private void createNotification(String informacja){
        Intent intent = new Intent(this, MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.icon);
        Notification noti = new NotificationCompat.Builder(this)
                .setContentTitle("Ostrzeżenie")
                .setContentText(informacja)
                .setTicker("")
                .setSmallIcon(R.drawable.icon)
                .setLargeIcon(icon)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, noti);


    }

    //-------------------------------------------------------------------------------


}
