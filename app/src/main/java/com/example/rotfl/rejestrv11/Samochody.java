package com.example.rotfl.rejestrv11;

/**
 * Created by rotfl on 28.10.2015.
 */
public class Samochody {


    /**
     * Id : 1
     * Nazwa : Scoda Octavia
     * Nr_Rejestracyjny : NE 12345
     * Url : http://www.skoda-auto.pl/shared/SiteCollectionImages/models/new-fabia/fabia/fabia/overview/fabia-overview-6reasons-bottom.jpg
     */

    private String Id;
    private String Nazwa;
    private String Nr_Rejestracyjny;
    private String Url;

    public void setId(String Id) {
        this.Id = Id;
    }

    public void setNazwa(String Nazwa) {
        this.Nazwa = Nazwa;
    }

    public void setNr_Rejestracyjny(String Nr_Rejestracyjny) {
        this.Nr_Rejestracyjny = Nr_Rejestracyjny;
    }

    public void setUrl(String Url) {
        this.Url = Url;
    }

    public String getId() {
        return Id;
    }

    public String getNazwa() {
        return Nazwa;
    }

    public String getNr_Rejestracyjny() {
        return Nr_Rejestracyjny;
    }

    public String getUrl() {
        return Url;
    }
}
