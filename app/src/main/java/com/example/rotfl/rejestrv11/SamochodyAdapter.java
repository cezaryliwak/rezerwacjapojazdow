package com.example.rotfl.rejestrv11;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by rotfl on 28.10.2015.
 */
public class SamochodyAdapter extends BaseAdapter {


    Context context;
    ArrayList<Samochody> listaSamochodow;
    int i = 0;
    ConfigClass cc = new ConfigClass();

    private static String url_wszystkei_folne ;
    SamochodyAdapter samochodyAdapter;
    MainActivity main = new MainActivity();
    public loginActivity login;

    String adapterIdSamochodu;
    String url;
    int licznikOdswiez; // potrzebne do warunku - kiedy ma odswiezac MainActivity po oddaniu samochodu


    Button oddajButton;
    AsyncHttpClient client;

    public ArrayList<WolneSamochody> wolneSamochodies;

    public SamochodyAdapter() {

    }

    public SamochodyAdapter(Context context, ArrayList<Samochody> listaSamochodow) {
        this.context = context;
        this.listaSamochodow = listaSamochodow;

    }

    @Override
    public int getCount() {

        return listaSamochodow.size();
    }

    @Override
    public Object getItem(int position) {
        return listaSamochodow.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);



        //Przypisanie wartosci do View
        final View rowView = inflater.inflate(R.layout.layout_single, parent, false);
        final Samochody samochody = (Samochody) getItem(position);

        main.setColor(rowView, listaSamochodow, Integer.parseInt(samochody.getId().toString()));

        ImageView imageView = (ImageView) rowView.findViewById(R.id.imageImageView);

        Picasso.with(context).load(samochody.getUrl()).into(imageView);
        final Button oddajButton = (Button) rowView.findViewById(R.id.oddajButton);
        TextView nazwa = (TextView) rowView.findViewById(R.id.nazwaTextView);
        final TextView nr_rej = (TextView) rowView.findViewById(R.id.Nr_rejestracyjnyTextView);


        nazwa.setText(samochody.getNazwa());
        nr_rej.setText(samochody.getNr_Rejestracyjny());


        login = new loginActivity();
        wolneSamochodies = new ArrayList();
//**************************************************************************************************************************
//wczytuje wszystkie zajete samochody po to aby wyswietlić przycisk do oddania samochodu przed czasem
        if (login.idUzytkownikaZalogowanego != null) {
            url_wszystkei_folne = cc.getIpAdress()+"/WyswietlZajete.php";
                AsyncHttpClient client2 = new AsyncHttpClient();
                client2.get(url_wszystkei_folne, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        InputStream inputStream = new ByteArrayInputStream(responseBody);

                        try {
                            inputStream.read();
                            inputStream.close();

                            String text = new String(responseBody);

                            Gson gson = new Gson();
                            com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
                            JsonArray jsonArray = jsonParser.parse(text).getAsJsonArray();

                                for (JsonElement jsonElement : jsonArray) {
                                    WolneSamochody wSamochody = gson.fromJson(jsonElement, WolneSamochody.class);
                                    wolneSamochodies.add(wSamochody);
                                }

//**************************************************************************************************************************
//jesli te warunki są spełnione to na zajetym samochodzie, który jest zajety przez
// uzytkownika wyswietli sie przycisk z możliwoscia oddania samochodu
                            for (WolneSamochody ws : wolneSamochodies) {
                                if (login.idUzytkownikaZalogowanego.equals(ws.getId_Uzytkownika())) {
                                    if (samochody.getId().equals(ws.getId_Samochodu())) {
                                        oddajButton.setVisibility(View.VISIBLE);

                                    }
                                }
                            }
//**************************************************************************************************************************

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    }
                });
//**************************************************************************************************************************
//**************************************************************************************************************************
//Update do bazy danych z datą jaka jest w momencie klikniecia w przycisk oddaj
            oddajButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    client = new AsyncHttpClient();
                    for(WolneSamochody w :wolneSamochodies){
                        if(samochody.getId().equals(w.getId_Samochodu())){
                                url =cc.getIpAdress()+"/UpdateOddajSamochod.php?idrezerwacji="+w.getId();
                                final RequestHandle requestHandle = client.get(url, new AsyncHttpResponseHandler() {
                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                        InputStream inputStream = new ByteArrayInputStream(responseBody);
                                        try {
                                            licznikOdswiez=1;
                                            inputStream.read();
                                            inputStream.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    }
                                });
                        }
                    }

                }
            });
//**************************************************************************************************************************
        }

        return rowView;
    }//******************KONIEC GETVIEW****************************
}


