package com.example.rotfl.rejestrv11;

import java.io.Serializable;

/**
 * Created by rotfl on 29.10.2015.
 */
public class Uzytkownicy implements Serializable{


    private String id;
    private String imie;
    private String nazwisko;
    private String login;
    private String password;

    public void setId(String id) {
        this.id = id;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
