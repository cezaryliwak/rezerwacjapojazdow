package com.example.rotfl.rejestrv11;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by rotfl on 29.10.2015.
 */
public class UzytkownicyAdapter {

    Context context;
    ArrayList<Uzytkownicy> listaUzytkownikow;

    public ArrayList<Uzytkownicy> getListaUzytkownikow() {
        return listaUzytkownikow;
    }

    public void setListaUzytkownikow(ArrayList<Uzytkownicy> listaUzytkownikow) {
        this.listaUzytkownikow = listaUzytkownikow;
    }



    public UzytkownicyAdapter(Context context, ArrayList<Uzytkownicy> listaUzytkownikow) {
        this.context = context;
        this.listaUzytkownikow = listaUzytkownikow;
    }

    public UzytkownicyAdapter(){

    }

    public int getCount() {
        return listaUzytkownikow.size();
    }


    public Object getItem(int position) {
        return listaUzytkownikow.get(position);
    }


//Layout methods



    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);

        return null; //tymczasowa wartosc
    }
}

