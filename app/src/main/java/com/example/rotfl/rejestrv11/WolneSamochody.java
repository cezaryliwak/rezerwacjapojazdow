package com.example.rotfl.rejestrv11;

/**
Klasa ma mylącą nazwę - jest ona widokiem samochodów które są aktualnie zajęte!!!
 */
public class WolneSamochody {


    /**
     * Id : 43
     * Id_Samochodu : 4
     * Id_Uzytkownika : 2
     * Data_Rezerwacji_Od : 2015-11-11 10:15:00
     * Data_Rezerwacji_Do : 2015-11-17 10:54:23
     * uzytkownikId : 2
     * imie : Pawe?
     * nazwisko : Kowalski
     */

    private String Id;
    private String Id_Samochodu;
    private String Id_Uzytkownika;
    private String Data_Rezerwacji_Od;
    private String Data_Rezerwacji_Do;
    private String uzytkownikId;
    private String imie;
    private String nazwisko;

    public void setId(String Id) {
        this.Id = Id;
    }

    public void setId_Samochodu(String Id_Samochodu) {
        this.Id_Samochodu = Id_Samochodu;
    }

    public void setId_Uzytkownika(String Id_Uzytkownika) {
        this.Id_Uzytkownika = Id_Uzytkownika;
    }

    public void setData_Rezerwacji_Od(String Data_Rezerwacji_Od) {
        this.Data_Rezerwacji_Od = Data_Rezerwacji_Od;
    }

    public void setData_Rezerwacji_Do(String Data_Rezerwacji_Do) {
        this.Data_Rezerwacji_Do = Data_Rezerwacji_Do;
    }

    public void setUzytkownikId(String uzytkownikId) {
        this.uzytkownikId = uzytkownikId;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getId() {
        return Id;
    }

    public String getId_Samochodu() {
        return Id_Samochodu;
    }

    public String getId_Uzytkownika() {
        return Id_Uzytkownika;
    }

    public String getData_Rezerwacji_Od() {
        return Data_Rezerwacji_Od;
    }

    public String getData_Rezerwacji_Do() {
        return Data_Rezerwacji_Do;
    }

    public String getUzytkownikId() {
        return uzytkownikId;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }
}
