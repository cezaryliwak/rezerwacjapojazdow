package com.example.rotfl.rejestrv11;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by rotfl on 06.11.2015.
 */
public class WolneSamochodyAdapter extends BaseAdapter {

    Context context;
    public ArrayList<WolneSamochody> lostaWolnychSamochodow;


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ArrayList<WolneSamochody> getLostaWolnychSamochodow() {
        return lostaWolnychSamochodow;
    }

    public void setLostaWolnychSamochodow(ArrayList<WolneSamochody> lostaWolnychSamochodow) {
        this.lostaWolnychSamochodow = lostaWolnychSamochodow;
    }

    public WolneSamochodyAdapter(Context context, ArrayList<WolneSamochody> lostaWolnychSamochodow) {
        this.context = context;
        this.lostaWolnychSamochodow = lostaWolnychSamochodow;


    }

    public WolneSamochodyAdapter() {

    }
    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;

    }

}
