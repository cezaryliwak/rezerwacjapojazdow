package com.example.rotfl.rejestrv11;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import org.apache.http.Header;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by rotfl on 28.10.2015.
 */
public class loginActivity extends Activity {


    EditText logEditText;
    EditText pasEditText;
    TextView t ;
    TextView errorLogin;
    Button zalogujButton;
    AsyncHttpClient client;
    ConfigClass cc = new ConfigClass();
    private static final String PREFERENCES_NAME = "idUzytkownika";
    private SharedPreferences preferences;
    public static final String USER_NAME = "Username";
    static String url_all_users;
     String username;
    public String password;
    public TempData td;
    public static String idUzytkownikaZalogowanego;



    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        preferences = getSharedPreferences(PREFERENCES_NAME, Activity.MODE_PRIVATE);


        logEditText = (EditText) findViewById(R.id.loginEditText);
        pasEditText = (EditText) findViewById(R.id.passwordEditText);
        zalogujButton = (Button) findViewById(R.id.zalogujButton);
        errorLogin = (TextView) findViewById(R.id.errorLogin);

        client = new AsyncHttpClient();

        zalogujButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String a;
                username = logEditText.getText().toString();
                password = pasEditText.getText().toString();
                url_all_users = cc.getIpAdress()+"/login.php?login=" + username + "&password=" + password;
                RequestHandle requestHandle = client.get(url_all_users, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        InputStream inputStream = new ByteArrayInputStream(responseBody);
                        ArrayList<Uzytkownicy> uzytkownicies = new ArrayList();
                        try {
                            inputStream.read();
                            inputStream.close();
                            String text = new String(responseBody);
                            Gson gson = new Gson();
                            com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
                            JsonArray jsonArray = jsonParser.parse(text).getAsJsonArray();
                            for (JsonElement jsonElement : jsonArray) {
                                Uzytkownicy uzytkownicy = gson.fromJson(jsonElement, Uzytkownicy.class);
                                uzytkownicies.add(uzytkownicy);
                            }
                            System.out.println("============================" + uzytkownicies.size());
                            if (uzytkownicies.size() == 1) {
                                Uzytkownicy u = uzytkownicies.get(0);
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                i.putExtra("idUzytkownika", u.getId());
                                i.putExtra("uzyt", u);
                                i.putExtra("imie", u.getImie());
                                idUzytkownikaZalogowanego = u.getId();
                                //startMyService();
                                startMyService(idUzytkownikaZalogowanego);       //---->uruchomienie serwisu odpowiedzialnego za powiadomienie
                                finish();
                                startActivity(i);
                            } else {
                                errorLogin.setText("Podałeś błędny login lub hasło.");
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    }
                });
            }
        });
    }


    private void startMyService(String id){
        Intent serviceIntent = new Intent(this, RssPullService.class);
        System.out.println(idUzytkownikaZalogowanego + " '''''''''''''''''''''''''''''''''''''''' " + id);
        serviceIntent.putExtra("idUzytkownika01", id);
        startService(serviceIntent);
    }
}
